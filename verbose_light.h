/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_VERBOSE_LIGHT_H_INCLUDED
# define YY_YY_VERBOSE_LIGHT_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    _CALL_KW = 258,
    _THEN_KW = 259,
    _END_KW = 260,
    _FUNCTION_KW = 261,
    _WITH_KW = 262,
    _PROJECTS_TO = 263,
    _AS_KW = 264,
    _COMMA = 265,
    _HASH = 266,
    _LOCAL_KW = 267,
    _IF_KW = 268,
    _UNLESS_KW = 269,
    _WHILE_KW = 270,
    _UNTIL_KW = 271,
    _ID = 272,
    _STRING = 273,
    _NUMERIC = 274,
    _FALSE = 275,
    _TRUE = 276,
    _INDENT = 277,
    _OUTDENT = 278,
    _PRINT_OP = 279,
    _ASSIGN_OP = 280,
    _PLUS_OP = 281,
    _MINUS_OP = 282,
    _MUL_OP = 283,
    _DIV_OP = 284,
    _AND_OP = 285,
    _OR_OP = 286,
    _EQUALS_OP = 287,
    _NOT_OP = 288,
    _OF_OP = 289
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 49 "verbose_light.y" /* yacc.c:1909  */

  int numeric;
  char* string;
  struct Node* node;
  struct Value* value;

#line 96 "verbose_light.h" /* yacc.c:1909  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_VERBOSE_LIGHT_H_INCLUDED  */
