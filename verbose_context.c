#include "verbose_context.h"
#include "verbose_syntax.h"
#include <stdlib.h>
#include <stdio.h>
struct Context* contextStack;

void freeContext(struct Context* context) {
  struct ContextVariable* vars = context -> variables;
  if (!vars) return;
  
  do {
    struct ContextVariable* next = vars -> next;
    free(vars);
    vars = next;
  } while (vars);
}

void pushNewContextStack() {
  struct Context* newContext = malloc(sizeof(struct Context));
  newContext -> prev = contextStack;
  newContext -> functions = NULL;
  newContext -> variables = NULL;
  contextStack = newContext;
}

void popContextStack() {
  struct Context* current = contextStack;
  contextStack = current -> prev;
  freeContext(current);
}

struct Context* getCurrentContext() {
  return contextStack;
}

void initializeGlobalContext() {
  struct Context* context = malloc(sizeof(struct Context));
  context -> prev = (struct Context*)0xDEADBEEF;
  context -> functions = NULL;
  context -> variables = NULL;
  contextStack = context;
}

struct ContextVariable* getPreferableLocal(char* id) {
  if (getCurrentContext() -> variables) {
    struct ContextVariable* last = getCurrentContext() -> variables;
    if (!strcmp(last -> id, id)) { return last; }
    
    while (last -> next) {
      last = last -> next;
      if (!strcmp(last -> id, id)) {  return last; }
    }

    last -> next = malloc(sizeof(struct ContextVariable));
    last -> next -> id = id;
    last -> next -> next = NULL;
    return last -> next;   
  } else {
    getCurrentContext() -> variables = malloc(sizeof(struct ContextVariable));
    getCurrentContext() -> variables -> id = id;
    getCurrentContext() -> variables -> next = NULL;
    return getCurrentContext() -> variables;
  }
}

struct ContextVariable* getPreferable(struct Context* context, char* id) {
  if (context == (struct Context*)0xDEADBEEF) {
    return getPreferableLocal(id);
  }

  if (context -> variables) {
    struct ContextVariable* last = context -> variables;
    
    if (!strcmp(last -> id, id)) { return last; }

    while (last -> next) {
      last = last -> next;
      if (!strcmp(last -> id, id)) {  return last; }
    }
  }
  
  return getPreferable(context -> prev, id);
}

struct ContextFunction* getPreferableLocalFunction(char* id) {
  if (getCurrentContext() -> functions) {
    struct ContextFunction* last = getCurrentContext() -> functions;

    if (!strcmp(last -> id, id)) { return last; }
    
    while (last -> next) {
      last = last -> next;
      if (!strcmp(last -> id, id)) {  return last; }
    }
    last -> next = malloc(sizeof(struct ContextFunction));
    last -> next -> id = id;
    return last -> next;
    
  } else {
    getCurrentContext() -> functions = malloc(sizeof(struct ContextFunction));
    getCurrentContext() -> functions -> id = id;
    return getCurrentContext() -> functions;
  }
}

struct ContextFunction* retrieveFunctionRecursive(char* id, struct Context* context) {
  struct ContextFunction* funcs = context -> functions;
  while (funcs) {
    if (!strcmp(funcs -> id, id)) return funcs;
    funcs = funcs -> next;
  }

  if (context -> prev != (struct Context*)0xDEADBEEF) {
    return retrieveFunctionRecursive(id, context -> prev);
  } else {
    return NULL;
  }
}

struct ContextFunction* retrieveFunction(char* id) {
  struct ContextFunction* funcs = getCurrentContext() -> functions;
  while (funcs) {
    if (!strcmp(funcs -> id, id)) return funcs;
    funcs = funcs -> next;
  }

  if (getCurrentContext() -> prev != (struct Context*)0xDEADBEEF) {
    return retrieveFunctionRecursive(id, getCurrentContext() -> prev);
  } else {
    return NULL;
  }    
}

void setVariable(char* id, struct Value* val) {
  struct ContextVariable* var = getPreferable(getCurrentContext(), id);
  var -> val = val;
}

void setLocalVariable(char* id, struct Value* val) {
  struct ContextVariable* var = getPreferableLocal(id);
  var -> val = val;
}

struct Value* resolveVariableRecursive(char* id, struct Context* context) {
  struct ContextVariable* vars = context -> variables;
  while (vars) {
    if (!strcmp(vars -> id, id)) return vars -> val;
    vars = vars -> next;
  }

  if (context -> prev != (struct Context*)0xDEADBEEF) {
    return resolveVariableRecursive(id, context -> prev);
  } else {
    struct Value* val = newValue();
    val -> type = NOTHING;
    return val;
  }
}

struct Value* resolveVariable(char* id) {
  struct Value* val = resolveVariableRecursive(id, getCurrentContext());
  return val;
}

void createLocalFunction(char* id, char* inId, struct Node* block) {
  struct ContextFunction* func = getPreferableLocalFunction(id);
  func -> function = block;
  func -> inId = inId;
}
