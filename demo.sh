#!/bin/sh
echo ====== Arithmetic expr. =======
echo source:
cat ./demo/arithmetic.verbose 
echo "\n"
./verbose_light ./demo/arithmetic.verbose
echo "\n"

echo =========== Hashes ============
echo source:
cat ./demo/hashes.verbose 
echo "\n"
./verbose_light ./demo/hashes.verbose
echo "\n"

echo ========= Functions ===========
echo source:
cat ./demo/functions.verbose 
echo "\n"
./verbose_light ./demo/functions.verbose

echo ==== Conditionals + loops =====
echo source:
cat ./demo/conditionals.verbose 
echo "\n"
./verbose_light ./demo/conditionals.verbose

echo ==== Functionality demo ====
echo source:
cat ./demo/simple.verbose 
echo "\n"
./verbose_light ./demo/simple.verbose

echo ==== Lambda expr. demo ====
echo source:
cat ./demo/lambdas.verbose 
echo "\n"
./verbose_light ./demo/lambdas.verbose