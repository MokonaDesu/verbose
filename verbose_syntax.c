#include "verbose_syntax.h"
#include "verbose_stacks.h"
#include <malloc.h>

struct Node* newSyntaxTree() {
  struct Node* tree = (struct Node*)malloc(sizeof(struct Node));
  tree -> type = FUNCTION_DEF;
  struct FDefData* data = (struct FDefData*)malloc(sizeof(struct FDefData));  
  tree -> data = data;
  return tree;
}

struct Value* newValue() {
  struct Value* val = (struct Value*)malloc(sizeof(struct Value));
  val -> type = NOTHING;
  return val;
}

struct Value* newHash() {
  struct Value* hash = (struct Value*)malloc(sizeof(struct Value));
  hash -> type = HASH;
  return hash;
}

struct Node* newNode() {
  return (struct Node*)malloc(sizeof(struct Node));
}
  
struct HashElement* rewindToEnd(struct Value* hash) {
  struct HashElement* current = (struct HashElement*)hash -> data;
  while (current -> next) {
    current = current -> next;
  }
  return current;
}
 
void appendToHash(struct Value* hash, char* id, struct Node* expr) {
  struct HashElement* nextElement = (struct HashElement*)malloc(sizeof(struct HashElement));
  nextElement -> data = expr;
  nextElement -> id = id;
    
  if (hash -> data) {
    struct HashElement* target = rewindToEnd(hash);
    target -> next = nextElement;
  } else {
    hash -> data = nextElement;
  }
}

struct Node* makeFunctionCall(char* id, struct Value* inputHash) {
  struct Node* node = newNode();
  node -> type = FUNCTION_CALL;
  struct FunctionCallData* data = (struct FunctionCallData*)malloc(sizeof(struct FunctionCallData));
  data -> id = id;
  data -> args = inputHash;
  node -> data = data;
  if (inputHash -> type != HASH && inputHash -> type != VARIABLE) {
    printf("error! Only hashes are suitable for function arguments.");
  }
  return node;
}

struct Node* wrapValue(struct Value* val) {
  struct Node* node = newNode();
  node -> type = VALUE;
  node -> data = val;
  return node;
}

struct Value* makeBoolean(int val) {
  struct Value* value = newValue();
  value -> type = LOGIC;
  value -> data = val ? (void*)LOGIC_TRUE : (void*)LOGIC_FALSE;
  return value;
}

struct Node* makeNot(struct Node* arg) {
  struct Node* node = newNode();
  node -> type = NOT;
  node -> data = arg;
  arg -> parent = node;
  return node;
}

struct Node* makeIdTarget(char* id, struct Node* target) {
  struct Node* node = newNode();
  struct IdTargetData* data = (struct IdTargetData*)malloc(sizeof(struct IdTargetData));
  data -> id = id;
  data -> target = target;
  node -> data = data;
  return node;
}

struct Node* makeAccessor(char* id, struct Node* target) {
  struct Node* node = makeIdTarget(id, target);
  node -> type = OF;
  target -> parent = node;
  return node;
}
 
struct Node* makeAssignment(char* id, struct Node* target) {
  struct Node* node = makeIdTarget(id, target);
  node -> type = ASSIGN;
  target -> parent = node;
  return node;
}

struct Node* makeLocalAssignment(char* id, struct Node* target) {
  struct Node* node = makeAssignment(id, target);
  node -> type = ASSIGN_LOCAL;
  return node;
}

struct Node* makeNodeNode(struct Node* expr1, struct Node* expr2, enum NodeType type) {
  struct Node* node = newNode();
  node -> type = type;
  struct NodeNodeData* data = (struct NodeNodeData*)malloc(sizeof(struct NodeNodeData));
  data -> node1 = expr1;
  expr1 -> parent = node;
  data -> node2 = expr2;
  expr2 -> parent = node;
  node -> data = data;
  return node;
}
 
struct Value* makeValue(void* data, enum ValueType type) {
  struct Value* val = newValue();
  val -> type = type;
  val -> data = data;
  return val;
}

struct Value* makeReader(char* id) {
  struct Value* val = newValue();
  val -> data = id;
  val -> type = VARIABLE;
  return val;
}

struct Node* makeFDef(char* id, char* inId, struct Node* block) {
  struct FDefData* blockData = (struct FDefData*)(block -> data); 
  blockData -> id = id;
  blockData -> inId = inId;
  return block;
}

struct Node* makePrint(struct Node* val) {
  struct Node* node = newNode();
  node -> data = val;
  node -> type = PRINT;
  return node;
}

struct Node* makeConditional(struct Node* condition, struct Node* block) {
  struct Node* node = newNode();
  struct NodeNodeData* data = (struct NodeNodeData*)malloc(sizeof(struct NodeNodeData));
  data -> node1 = condition;
  data -> node2 = block;
  node -> data = data;
  condition -> parent = node;
  return node;
}

struct Node* makeWhile(struct Node* condition, struct Node* block) {
  struct Node* node = makeConditional(condition, block);
  node -> type = WHILE;
  return node;
}

struct Node* makeUntil(struct Node* condition, struct Node* block) {
  return makeWhile(makeNot(condition), block);
}

struct Node* makeIf(struct Node* condition, struct Node* block) {
  struct Node* node = makeConditional(condition, block);
  node -> type = IF;
  return node;
}

struct Node* makeUnless(struct Node* condition, struct Node* block) {
  return makeIf(makeNot(condition), block);
}

void attachToCurrentBlock(struct Node* expr) {
  struct FDefData* data = (struct FDefData*)(peekTopNode() -> data);
  struct Node* currentBlock = data -> block;
  expr -> parent = peekTopNode();
  if (!currentBlock) {
    data -> block = expr;
    return;
  } 
  
  while (currentBlock -> nextSibling) {
    currentBlock = currentBlock -> nextSibling;
  }
  currentBlock -> nextSibling = expr;
  expr -> prevSibling = currentBlock;
}
