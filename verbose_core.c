#include "verbose_core.h"
#include "verbose_context.h"
#include "verbose_printer.h"
#include <string.h>
#include <malloc.h>

struct Value* evalNode(struct Node* node) {
  switch(node -> type) {
    case PLUS:
    case MINUS:
    case MUL:
    case DIV: {
      return evalArithmetic(((struct NodeNodeData*)(node -> data)), node -> type);
    }
    case OR:
    case AND: {
      return evalLogic(((struct NodeNodeData*)(node -> data)), node -> type);
    }
    case NOT: {
      return evalNot((struct Node*)(node -> data));
    }
    case FUNCTION_DEF: {
      struct FDefData* data = (struct FDefData*)node -> data;
      createLocalFunction(data -> id, data -> inId, data -> block);
      return newValue();
    }  
    case FUNCTION_CALL: {
      return evalFunctionCall((struct FunctionCallData*)node -> data);
    }
    case VALUE: {
      return unwrapVariable((struct Value*)node -> data);
    }
    case ASSIGN: {
      return evalAssignment((struct IdTargetData*)node -> data);
    }
    case ASSIGN_LOCAL: {
      return evalLocalAssignment((struct IdTargetData*)node -> data);
    }
    case EQUALS: {
      int result = checkEquality((struct NodeNodeData*)node -> data);
      return makeBoolean(result);
    }
    case PRINT: {
      struct Value* val = evalNode(node -> data);
      printPretty(val);
      return val;
    }
    case IF: {
      struct NodeNodeData* data = (struct NodeNodeData*)node -> data;
      return evalIf(data -> node1, data -> node2);
    }
    case WHILE: {
      struct NodeNodeData* data = (struct NodeNodeData*)node -> data;
      return evalWhile(data -> node1, data -> node2);
    }
    case OF: {
      return evalAccessor((struct IdTargetData*)node -> data);
    }
  }
}

struct Value* evalBlock(struct Node* block) {
  struct Value* lastValue; 
  do {
    lastValue = evalNode(block);
    block = block -> nextSibling;
    } while (block);

  return lastValue;
}

struct Value* evalFunctionCall(struct FunctionCallData* data) {
  struct Value* returnValue;
  struct ContextFunction* functionData = retrieveFunction(data -> id);
  if (!functionData) { printf("Call target \"%s\" is not defined.\n", data -> id); return newValue(); }
  pushNewContextStack();
  setLocalVariable(functionData -> inId, data -> args);
  returnValue = evalBlock(functionData -> function);
  popContextStack();
  return returnValue;
}

struct Value* unwrapVariable(struct Value* value) {
  if (value -> type == VARIABLE) {
    struct Value* result = resolveVariable((char*)(value -> data));
    return result;
  }
  return value;
}

int performArithmetic(enum NodeType type, int a, int b) {
  switch(type) {
  case PLUS: return a + b;
  case MINUS: return a - b;
  case MUL: return a * b;
  case DIV: return a / b;
  }
  return 0;
}

struct Value* evalArithmetic(struct NodeNodeData* data, enum NodeType type) {
  struct Value* val = newValue();
  val -> type = NOTHING;
  struct Value* val1 = evalNode(data -> node1);
  struct Value* val2 = evalNode(data -> node2);

  if (val1 -> type == NUMERIC && val2 -> type == NUMERIC) {
    val -> type = NUMERIC;
    int result = performArithmetic(type, *(int*)(val1 -> data), *(int*)(val2 -> data));
    val -> data = malloc(sizeof(int));
    *(int*)(val -> data) = result;
  } else {
    printf("Invalid argument types (got %s and %s)\n", typeName(val1 -> type), typeName(val2 -> type));
  }

  return val;
}

struct Node* findHashEntryByName(struct HashElement* hash, char* id) {
  while (hash) {
    if (!strcmp(hash -> id, id)) return hash -> data;
    hash = hash -> next;
  }
  return wrapValue(newValue());
}

struct Value* evalAccessor(struct IdTargetData* data) {
  struct Value* target = evalNode(data -> target);
  if (target -> type != HASH) {
    printf("Invalid accessor target! Got %s and expected hash.\n", typeName(target -> type));
    return newValue();
  } else {
    return evalNode(findHashEntryByName((struct HashElement*)target -> data, data -> id));
  }
}

struct Value* evalLocalAssignment(struct IdTargetData* data) {
  struct Value* result = evalNode(data -> target);
  setLocalVariable(data -> id, result);
  return result;  
}

struct Value* evalAssignment(struct IdTargetData* data) {
  struct Value* result = evalNode(data -> target);
  setVariable(data -> id, result);
  return result;  
}

int checkEquality(struct NodeNodeData* data) {
  struct Value* val1 = evalNode(data -> node1);
  struct Value* val2 = evalNode(data -> node2);
  
  if (val1 -> type == val2 -> type) {
    if (val1 -> type == NUMERIC) {
      return *(int*)(val1 -> data) == *(int*)(val2 -> data);
    } else if (val1 -> type == LOGIC) {
      return (val1 -> data) == (val2 -> data);
    } else if (val1 -> type == STRING) {
      return !strcmp((char*)(val1 -> data), (char*)(val2 -> data));
    }
  }
  return 0;
}

int asBoolean(struct Value* val) {
  if (val -> type == NUMERIC) {
    return *(int*)(val -> data) != 0;
  } else if (val -> type == LOGIC) {
    return val -> data == (void*)LOGIC_TRUE;
  } else if (val -> type == STRING) {
    return 1;
  } else if (val -> type == HASH) {
    return 1;
  } else return 0;
}

struct Value* evalLogic(struct NodeNodeData* data, enum NodeType type) {
  int val1 = asBoolean(evalNode(data -> node1));
  int val2 = asBoolean(evalNode(data -> node2));

  if (type == AND) {
    return makeBoolean(val1 && val2);
  } else {
    return makeBoolean(val1 || val2);
  }
}
  
struct Value* evalNot(struct Node* node) {
  return makeBoolean(1 - asBoolean(evalNode(node)));
}

struct Value* evalIf(struct Node* condition, struct Node* action) {
  if (asBoolean(evalNode(condition))) {
    pushNewContextStack();
    struct Value* result = evalBlock(((struct FDefData*)action -> data) -> block);
    popContextStack();
    return result;
  } else {
    return newValue();
  }
}

struct Value* evalWhile(struct Node* condition, struct Node* action) {
  struct Value* lastResult;
  while (asBoolean(evalNode(condition))) {
    pushNewContextStack();
    lastResult = evalBlock(((struct FDefData*)action -> data) -> block);
    popContextStack();
  }
  return lastResult;
}

void executeAST(struct Node* root) {
  if (root -> type != FUNCTION_DEF) { printf("Execution failed! Tree root has invalid type!\n"); return; }
  struct Node* block = ((struct FDefData*)root -> data) -> block;
  evalBlock(block);
}
