#ifndef VERBOSE_CONTEXT
#define VERBOSE_CONTEXT

struct Context {
  struct ContextVariable* variables;
  struct ContextFunction* functions;
  struct Context* prev;
};

struct ContextVariable {
  char* id;
  struct Value* val;
  struct ContextVariable* next;
};

struct ContextFunction {
  char* id;
  char* inId;
  struct Node* function;
  struct ContextFunction* next;
};

struct ContextFunction* retrieveFunction(char* id);
struct Value* resolveVariable(char* id);
void setVariable(char* id, struct Value* val);
void setLocalVariable(char* id, struct Value* val);
void pushNewContextStack();
void popContextStack();
void initializeGlobalContext();
void createLocalFunction(char* id, char* inId, struct Node* block);
#endif
