#include "verbose_printer.h"
#include "verbose_syntax.h"
#include <stdio.h>

void printNodeNode(struct Node* node, char sign) {
  struct NodeNodeData* data = (struct NodeNodeData*)node -> data;
  printf("{ ");
  printNode(data -> node1);
  printf(" }");
  printf("%c", sign);
  printf("{ ");
  printNode(data -> node2);
  printf(" }");
}

void printIdTarget(struct IdTargetData* node, char sign) {
  printf("%s %c", node -> id, sign);
  printNode(node -> target);
}

void printHash(struct HashElement* hash) {
  printf(" @(");
  while (hash) {
    printf("%s => ", hash -> id);
    printNode(hash -> data);
    printf(";");
    hash = hash -> next;
  }
  printf(")");
}

void printHashPretty(struct HashElement* hash) {
  printf("@(");
  while (hash) {
    printf("%s=>", hash -> id);
    struct Node* node = hash -> data;
    if (node -> type != VALUE) {
      printf("(expr...)");
    } else {
      printPretty(((struct Value*)(node -> data)));
    }
    printf(";");
    hash = hash -> next;
  }
  printf(")\n");
}

void printPretty(struct Value* val) {
  switch (val -> type) {
    case NUMERIC: {
      printf("%d\n", *(int*)(val -> data));
    } break;
    case STRING: {
      printf("%s\n", (char*)val -> data);
    } break;
    case HASH: {
      printHashPretty((struct HashElement*)val -> data);
    } break;
    case LOGIC: {
      printf("%s\n", val -> data == (void*)LOGIC_TRUE ? "true" : "false");
    } break;
    case NOTHING: {
      printf("(nothing)\n");
    } break;
  }
}

void printValue(struct Value* val) {
  switch (val -> type) {
    case NUMERIC: {
      printf(" (numeric)%d", *(int*)(val -> data));
    } break;
    case STRING: {
      printf(" (string)%s", (char*)val -> data);
    } break;
    case HASH: {
      printHash((struct HashElement*)val -> data);
    } break;
    case VARIABLE: {
      printf(" (var)%s", (char*)val -> data);
    } break;
    case LOGIC: {
      printf(" (%s) ", val -> data == (void*)LOGIC_TRUE ? "TRUE" : "FALSE");
    } break;
  }
}

void printFunctionCall(struct FunctionCallData* data) {
  printf(" (call %s with ", data -> id);
  printValue(data -> args);
  printf(" )");
}
  
void printConditional(struct NodeNodeData* data) {
  printNode(data -> node1);
  printf(") then (");
  printNode(data -> node2);
  printf(")\n");
}
 
void printNode(struct Node* node) {
  switch (node -> type) {
    case PLUS: {
      printNodeNode(node, '+');
    } break;
    case MINUS: {
      printNodeNode(node, '-');
    } break;
    case MUL: {
      printNodeNode(node, '*');
    } break;
    case DIV: {
      printNodeNode(node, '/');
    } break;
    case EQUALS: {
      printNodeNode(node, '=');
    } break;
    case OR: {
      printNodeNode(node, '|');
    } break;
    case AND: {
      printNodeNode(node, '&');
    } break;
    case NOT: {
      printf("!");
      printNode(node -> data);
    } break;
    case FUNCTION_DEF: {
      struct FDefData* data = (struct FDefData*)(node -> data);
      printf(" (def %s of %s)", data -> id, data -> inId);
      printAST(data -> block);
    } break;
    case ASSIGN: {
      printIdTarget((struct IdTargetData*)node -> data, 'S');
    } break;
    case VALUE: {
      printValue((struct Value*)node -> data);
    } break;
    case OF: {
      struct IdTargetData* data = (struct IdTargetData*)node -> data;
      printNode(data -> target);
      printf("[%s]", data -> id);
    } break;
    case IF: {
      printf("if (");
      printConditional((struct NodeNodeData*)node -> data);
    } break;
    case WHILE: {
      printf("while (");
      printConditional((struct NodeNodeData*)node -> data);
    } break;
    case FUNCTION_CALL: {
      printFunctionCall((struct FunctionCallData*)node -> data);
    } break;
    default: {
      printf(" (type=%d) ", node -> type);
    }
  }
}
 
void printAST(struct Node* node) {
  printf("(tree=0x%lX) {\n", (long unsigned int)node);
  do {
    printNode(node);
    printf("\n");
    node = node -> nextSibling;
  }
  while (node);
  printf(" }");
}

char* typeName(enum ValueType type) {
  switch (type) {
  case NUMERIC: return "numeric";
  case STRING: return "string";
  case HASH: return "hash";
  case VARIABLE: return "variable";
  case NOTHING: return "nothing";
  case LOGIC: return "logic";
  }

  return "(error)";
}
