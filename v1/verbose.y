%{
#include <stdio.h>
#include <string.h>

extern int yylex();
extern int yyparse();
extern FILE* yyin; 

int errors = 0;

void yyerror(const char *str) {
  fprintf(stderr, "error: %s\n", str);
  exit(1);
}

int main(int argc, char** argv) {
  if(argc < 2) {
    printf("No input file!\n");
    return 1;
  }
  
  yyin = fopen(argv[1], "r");
  yyparse();
  printf("\nDone!\n");
  if (errors) {
    printf("Found %d %s!\n", errors, errors == 1 ? "error" : "errors");
    return 2;
  }

  return 0;
}

%}

%union
{
  int numeric;
  char* string;
}

%token _CALL_KW _THEN_KW _END_KW _FUNCTION_KW _WITH_KW _PROJECTS_TO _COMMA _HASH
%token _IF_KW _UNLESS_KW _WHILE_KW _UNTIL_KW

%token <string> _ID
%token <string> _STRING
%token <numeric> _NUMERIC
%token _INDENT _OUTDENT

%left _ASSIGN_OP
%left _PLUS_OP _MINUS_OP
%left _MUL_OP _DIV_OP
%left _MOD_OP
%left _NOT_OP _INC_OP _DEC_OP
%left _AND_OP _OR_OP _NOR_OP _XOR_OP _XNOR_OP _EQUALS_OP _NOT_EQ_OP
%left _OF_OP

%%
axiom: /* Lambda-rule */ | expr axiom

function_call: _CALL_KW expr _WITH_KW hash

multiple_expr: expr | expr multiple_expr

end: _END_KW                                          { printf("(end) "); }

begin: _THEN_KW                                       { printf("(begin) "); }

block: begin multiple_expr end

conditional: _IF_KW expr block                        { printf("(if) "); }
           | _UNLESS_KW expr block                    { printf("(unless) "); } 
           | _WHILE_KW expr block                     { printf("(while) "); }
           | _UNTIL_KW expr block                     { printf("(until) "); }

function_def: _FUNCTION_KW _OF_OP id block

key_value_pair: id _PROJECTS_TO expr

key_value_pairs: key_value_pair
               | key_value_pair _COMMA key_value_pairs
;

id: _ID                                                { printf("(id=%s) ", $1); }

hash_start: _HASH                                      { printf("(hash_start) "); }

hash: hash_start _INDENT _OUTDENT                      { printf("(hash)\n"); }
    | hash_start _INDENT key_value_pairs _OUTDENT      { printf("(hash) "); }

val: _NUMERIC      { printf("(num=%d) ", $1); }
   | _STRING       { printf("(str=%s) ", $1); }
   | id
   | hash          
   | function_call { printf("(call)\n"); }
;

expr: expr _MUL_OP expr    { printf("mul "); }
    | expr _DIV_OP expr    { printf("div "); }
    | expr _PLUS_OP expr   { printf("plus "); }
    | expr _MINUS_OP expr  { printf("minus "); }
    | expr _MOD_OP expr    { printf("modulus "); } 
    | expr _AND_OP expr    { printf("and "); }
    | expr _OR_OP expr     { printf("or "); }
    | expr _XOR_OP expr    { printf("xor "); }
    | expr _NOR_OP expr    { printf("nor "); }
    | expr _XNOR_OP expr   { printf("xnor "); }
    | expr _EQUALS_OP expr { printf("eq "); }
    | expr _ASSIGN_OP expr { printf("set "); }
    | expr _NOT_EQ_OP expr { printf("neq "); }
    | expr _OF_OP expr     { printf("of "); }
    | _INC_OP expr         { printf("++ "); }
    | _DEC_OP expr         { printf("-- "); }
    | _NOT_OP expr         { printf("not ");}
    | _INDENT expr _OUTDENT
    | function_def         { printf("func_def "); }
    | conditional          
    | val
;
%%
