#include "verbose_syntax.h"
#include "verbose_stacks.h"
#include <malloc.h>

struct CodeStack* codeBlocks;
struct HashStack* hashes;

struct Node* getCurrentBlock() {
  return codeBlocks -> node;
}

struct Value* getCurrentHash() {
  return hashes -> hash;
}

struct Node* popBlock() {
  struct CodeStack* currentBlock = codeBlocks;
  struct Node* currentNode = currentBlock -> node;
  free(currentBlock);
  codeBlocks = codeBlocks -> prev;
  return currentNode;    
}

struct Value* popHash() {
  struct HashStack* currentHash = hashes;
  struct Value* currentVal = hashes -> hash;
  free(currentHash);
  hashes = hashes -> prev;
  return currentVal;
}
 
void pushToCodeStack(struct Node* node) {
  struct CodeStack* newTop = malloc(sizeof(struct CodeStack));
  newTop -> node = node;
  newTop -> prev = codeBlocks;
  codeBlocks = newTop;
}

void pushToHashStack(struct Value* val) {
  struct HashStack* newTop = malloc(sizeof(struct HashStack));
  newTop -> hash = val;
  newTop -> prev = hashes;
  hashes = newTop;
}

struct Node* peekTopNode() {
  return codeBlocks -> node;
}

struct Value* peekTopHash() {
  return hashes -> hash;
}
