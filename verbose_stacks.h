#ifndef VERBOSE_STACKS
#define VERBOSE_STACKS

struct HashElement* rewindToEnd(struct Value* hash);
void appendToHash(struct Value* hash, char* id, struct Node* expr);
struct Node* getCurrentBlock();
struct Node* popBlock();
struct Value* popHash();
void pushToCodeStack(struct Node* node);
struct Node* peekTopNode();
struct Value* peekTopHash();
void appendToHash(struct Value* hash, char* id, struct Node* expr);
struct HashElement* rewindToEnd(struct Value* hash);

#endif
