#ifndef VERBOSE_SYNTAX_H
#define VERBOSE_SYNTAX_H

#define LOGIC_TRUE 1
#define LOGIC_FALSE 0

enum ValueType {
  NUMERIC,
  STRING,
  HASH,
  VARIABLE,
  NOTHING,
  LOGIC
};

enum NodeType {
  VALUE,
  
  FUNCTION_DEF,
  FUNCTION_CALL,
  PRINT,
  
  PLUS,
  MINUS,
  MUL,
  DIV,

  ASSIGN,
  ASSIGN_LOCAL,
  EQUALS,
  NOT,
  OR,
  AND,
  OF,

  IF,
  WHILE
};

struct Node {
  enum NodeType type;
  void* data;
  
  struct Node* nextSibling;
  struct Node* prevSibling;
  struct Node* parent;
};

struct Value {
  enum ValueType type;
  void* data;
};

struct HashElement {
  char* id;
  struct Node* data;
  struct HashElement* next;
};

struct CodeStack {
  struct Node* node;
  struct CodeStack* prev;
};

struct HashStack {
  struct Value* hash;
  struct HashStack* prev;
};

struct FDefData {
  char* id;
  char* inId;
  struct Node* block;
};
 
struct IdTargetData {
  char* id;
  struct Node* target;    
};

struct NodeNodeData {
  struct Node* node1;
  struct Node* node2;
};
 
struct FunctionCallData {
  char* id;
  struct Value* args;
};

/* Nodes manipulation functions and constructors*/

void attachToCurrentBlock(struct Node* expr);
struct Node* newSyntaxTree();
struct Node* newNode();
struct Node* makeFunctionCall(char* id, struct Value* inputHash);
struct Node* wrapValue(struct Value* val);
struct Node* makeNot(struct Node* arg);
struct Node* makePrint(struct Node* val);
struct Node* makeIdTarget(char* id, struct Node* target);
struct Node* makeAccessor(char* id, struct Node* target);
struct Node* makeAssignment(char* id, struct Node* target);
struct Node* makeLocalAssignment(char* id, struct Node* target);
struct Node* makeNodeNode(struct Node* expr1, struct Node* expr2, enum NodeType type);
struct Node* makeFDef(char* id, char* inId, struct Node* block);
struct Node* makeConditional(struct Node* condition, struct Node* block);
struct Node* makeWhile(struct Node* condition, struct Node* block);
struct Node* makeUntil(struct Node* condition, struct Node* block);
struct Node* makeIf(struct Node* condition, struct Node* block);
struct Node* makeUnless(struct Node* condition, struct Node* block);

/* Value constructors*/

struct Value* newValue();
struct Value* newHash();
struct Value* makeValue(void* data, enum ValueType type);
struct Value* makeReader(char* id);
struct Value* makeBoolean(int val);

#endif
