all:
	bison -v --defines=verbose_light.h verbose_light.y
	lex verbose_light.lex
	gcc lex.yy.c verbose_light.h verbose_core.c verbose_syntax.c verbose_stacks.c verbose_printer.c verbose_context.c -o verbose_light -lfl

run:
	bison -v --defines=verbose_light.h verbose_light.y
	lex verbose_light.lex
	gcc lex.yy.c verbose_light.h verbose_core.c verbose_syntax.c verbose_stacks.c verbose_printer.c verbose_context.c -o verbose_light -lfl
	./verbose_light test.verbose

demo:
	sh ./demo/demo.sh
