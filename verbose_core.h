#ifndef H_VERBOSE_CORE
#define H_VERBOSE_CORE

#include "verbose_syntax.h"

void executeAST(struct Node* root);

int performArithmetic(enum NodeType type, int a, int b);
int checkEquality(struct NodeNodeData* data);
int asBoolean(struct Value* val);

struct Value* evalNode(struct Node* node);
struct Value* evalBlock(struct Node* block);
struct Value* unwrapVariable(struct Value* value);
struct Value* evalArithmetic(struct NodeNodeData* data, enum NodeType type);
struct Value* execFunction(void* callTarget);
struct Value* evalAssignment(struct IdTargetData* data);
struct Value* evalFunctionCall(struct FunctionCallData* data);
struct Value* evalLocalAssignment(struct IdTargetData* data);
struct Value* evalLogic(struct NodeNodeData* data, enum NodeType type);
struct Value* evalNot(struct Node* node);
struct Value* evalIf(struct Node* condition, struct Node* action);
struct Value* evalWhile(struct Node* condition, struct Node* action);
struct Value* evalAccessor(struct IdTargetData* data);

#endif
