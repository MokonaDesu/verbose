%{
#define PRINT_AST 0
  
#include <stdio.h>
#include <string.h>

#include "verbose_syntax.h"
#include "verbose_core.h"
#include "verbose_stacks.h"
#include "verbose_printer.h"
 
extern int yylex();
extern int yyparse();
extern FILE* yyin;
struct Node* lastBlock;
 
void yyerror(const char *str) {
  fprintf(stderr, "error: %s\n", str);
  exit(1);
}

void printASTIfNeeded() {
  if (PRINT_AST) {
    printf("\n\nAST:\n");
    printNode(peekTopNode());
    printf("\n");
  }
}
 
int main(int argc, char** argv) {
  pushToCodeStack(newSyntaxTree());
  
  if(argc < 2) {
    printf("No input file!\n");
    return 1;
  }
  
  yyin = fopen(argv[1], "r");
  yyparse();
  printASTIfNeeded();
  initializeGlobalContext();
  executeAST(peekTopNode());
  return 0;
}

%}

%union
{
  int numeric;
  char* string;
  struct Node* node;
  struct Value* value;
}

%token _CALL_KW _THEN_KW _END_KW _FUNCTION_KW _WITH_KW _PROJECTS_TO _AS_KW _COMMA _HASH _LOCAL_KW
%token _IF_KW _UNLESS_KW _WHILE_KW _UNTIL_KW

%token <string> _ID
%token <string> _STRING
%token <numeric> _NUMERIC
%token _FALSE _TRUE
%token _INDENT _OUTDENT

%left _PRINT_OP
%left _ASSIGN_OP
%left _PLUS_OP _MINUS_OP
%left _MUL_OP _DIV_OP
%left _AND_OP _OR_OP
%left _EQUALS_OP
%left _NOT_OP
%left _OF_OP

%type<node> end expr function_call function_def block conditional
%type<string> id
%type<value> val hash logic

%%
axiom: /* Lambda-rule */
       | axiom expr                                   { attachToCurrentBlock($2); }
       | axiom function_def                           { attachToCurrentBlock($2); }

function_call: _CALL_KW id _WITH_KW hash              { $$ = makeFunctionCall($2, $4); }

multiple_expr: expr                                   { attachToCurrentBlock($1); }
             | multiple_expr expr                     { attachToCurrentBlock($2); }

end: _END_KW                                          { $$ = popBlock(); }

begin: _THEN_KW                                       { pushToCodeStack(newSyntaxTree()); }

block: begin multiple_expr end                        { $$ = $3; }

conditional: _IF_KW expr block                        { $$ = makeIf($2, $3); } 
           | _UNLESS_KW expr block                    { $$ = makeUnless($2, $3); }
           | _WHILE_KW expr block                     { $$ = makeWhile($2, $3); }
           | _UNTIL_KW expr block                     { $$ = makeUntil($2, $3); }

function_def: id _AS_KW _FUNCTION_KW _OF_OP id block  { $$ = makeFDef($1, $5, $6); }

key_value_pair: id _PROJECTS_TO expr                  { appendToHash(peekTopHash(), $1, $3); }

key_value_pairs: key_value_pair                       
               | key_value_pair _COMMA key_value_pairs
;

id: _ID                                                { $$ = $1; }

hash_start: _HASH                                      { pushToHashStack(newHash()); }

hash: hash_start _INDENT _OUTDENT                      { $$ = popHash(); }
    | hash_start _INDENT key_value_pairs _OUTDENT      { $$ = popHash();}

logic: _TRUE                                           { $$ = makeBoolean(1); }
     | _FALSE                                          { $$ = makeBoolean(0); }

val: _NUMERIC                                          { int* data = malloc(sizeof(int)); *data = $1; $$ = makeValue(data, NUMERIC); }
   | _STRING                                           { $$ = makeValue($1, STRING); }
   | id                                                { $$ = makeReader($1); }
   | hash                                              { $$ = $1; }
   | logic                                             { $$ = $1; }
;

expr: expr _MUL_OP expr                                { $$ = makeNodeNode($1, $3, MUL); }
    | expr _DIV_OP expr                                { $$ = makeNodeNode($1, $3, DIV); }
    | expr _PLUS_OP expr                               { $$ = makeNodeNode($1, $3, PLUS); }
    | expr _MINUS_OP expr                              { $$ = makeNodeNode($1, $3, MINUS); }
    | expr _AND_OP expr                                { $$ = makeNodeNode($1, $3, AND); }
    | expr _OR_OP expr                                 { $$ = makeNodeNode($1, $3, OR); }
    | expr _EQUALS_OP expr                             { $$ = makeNodeNode($1, $3, EQUALS); }
    | id _ASSIGN_OP expr                               { $$ = makeAssignment($1, $3); }
    | _LOCAL_KW id _ASSIGN_OP expr                     { $$ = makeLocalAssignment($2, $4); }
    | id _OF_OP expr                                   { $$ = makeAccessor($1, $3); }
    | _NOT_OP expr                                     { $$ = makeNot($2); }
    | _PRINT_OP expr                                   { $$ = makePrint($2); }
    | _INDENT expr _OUTDENT                            { $$ = $2; }
    | conditional                                      { $$ = $1; }
    | function_call                                    { $$ = $1; }
    | val                                              { $$ = wrapValue($1); }
;
%%
