%{ 
#include <stdio.h>
#include "verbose_light.h"
#include "verbose_light.tab.c"

#define LEXER_DEBUG 0
%}

/* Lexemes */
ID               [A-z_][A-z0-9_]*
NUMERIC          [0-9]+
STRING           '.*'
TRUE             (true|yes|on)
FALSE            (false|no|off)

/* Operators */
PLUS_OP          \+
MINUS_OP         \-
MUL_OP           \*
DIV_OP           \/

ASSIGN_OP        (set|=)
EQUALS_OP        is
NOT_OP           not
PRINT_OP         (puts|print|echo)
AND_OP           and
OR_OP            or
OF_OP            of

/* Keywords and aux. */
INDENT           \(
OUTDENT          \)
PROJECTS_TO      (=>)
THEN_KW          (then|do)
IF_KW            (if)
UNLESS_KW        (unless)
WHILE_KW         (while)
UNTIL_KW         (until)
FUNCTION_KW      (function|lambda)
END_KW           (end)
LOCAL_KW         (local)
COMMA            ,
WITH_KW          (with)
CALL_KW          (call|exec)
WS               [ \n\r\t]
AS_KW            (as)
HASH             (@)
COMMENT          #.*

%%
{COMMENT}      /* Ignore comments */

{PLUS_OP}      return _PLUS_OP;
{MINUS_OP}     return _MINUS_OP;
{MUL_OP}       return _MUL_OP;
{DIV_OP}       return _DIV_OP;

{ASSIGN_OP}    return _ASSIGN_OP;
{EQUALS_OP}    return _EQUALS_OP;
{PRINT_OP}     return _PRINT_OP;
{NOT_OP}       return _NOT_OP;
{AND_OP}       return _AND_OP;
{OR_OP}        return _OR_OP;
{OF_OP}        return _OF_OP;

{CALL_KW}      return _CALL_KW;
{IF_KW}        return _IF_KW;
{UNLESS_KW}    return _UNLESS_KW; 
{WHILE_KW}     return _WHILE_KW;
{UNTIL_KW}     return _UNTIL_KW;
{WITH_KW}      return _WITH_KW;
{LOCAL_KW}     return _LOCAL_KW;
{AS_KW}        return _AS_KW;
{PROJECTS_TO}  return _PROJECTS_TO;
{COMMA}        return _COMMA;
{HASH}         return _HASH;
{FUNCTION_KW}  return _FUNCTION_KW;
{END_KW}       return _END_KW;
{THEN_KW}      return _THEN_KW;

{TRUE}         { return _TRUE; }
{FALSE}        { return _FALSE; }
{ID}           { yylval.string = strdup(yytext); return _ID; }
{NUMERIC}      { yylval.numeric = atoi(yytext); return _NUMERIC; }
{STRING}       { yylval.string = strdup(yytext); return _STRING; }

{INDENT}       return _INDENT;
{OUTDENT}      return _OUTDENT;

{WS}+          /* Ignore whitespace */
%%
