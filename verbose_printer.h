#ifndef VERBOSE_PRINTER
#define VERBOSE_PRINTER

#include "verbose_syntax.h"

void printNode(struct Node*);
void printNodeNode(struct Node* node, char sign);
void printIdTarget(struct IdTargetData* node, char sign);
void printHash(struct HashElement* hash);
void printValue(struct Value* val);
void printPretty(struct Value* val);
void printHashPretty(struct HashElement* hash);
void printFunctionCall(struct FunctionCallData* data);
void printConditional(struct NodeNodeData* data);
void printAST(struct Node*);
void printNode(struct Node* node);
char* typeName(enum ValueType type);

#endif
